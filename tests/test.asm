##############################################
# Fall 2015, CIS 314 Computer Organization   #
# Major Class Project                        #
##############################################

# This tests for function calls #############
# By calculating (a*b) , (a* (b-1)), ...(a*1)
#############################################
	
# Setting the stack pointer #
		#add $t1, $zero, $zero
		addi $t0, $zero, 7
		sw $t0, 0($t1) #put 7 at 0
		lw $t3, 0($t1) #put 7 in t3 from 0
		#sw $t3, 1($t1) #put 7 at 1
		#lw $t4, 1($t1) #put 7 in t4 from 1
	# branch to end
#############################

